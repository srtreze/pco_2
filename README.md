# 2nd PCO Project
### Grade - 22 (due to extras)

## A simple space race game in Java

In this game, each race can have more than one player, and it's theire objective to pass through numbered _way-points_ in succession, winning the first player to arrive.
The game has 7 levels and in every level there is a human player (represented by a space ship), that's controled via the keyboard, a simple npc and several obstacls. The levels get progresevly harder.
A skeleton Eclipse project was provided to us.

![Printscreen of the game](img/print.png)

### Playing

To start the game, rum _src/spacerace/Program.java_.

#### Controling the human player:

- arrow up - accelarates 1 unit;
- arrow down - decelarates 1 unit;
- arrow left - turns left 10 degrees;
- arrow right - turns right 10 degrees.

#### Outras teclas:
- 0-9 - loads the corresponding level;
- B -  turns on/off the backgrand image;
- M - turns on/off the backgrand music;
- S -  turns on/off the sound effects;
- Q - quits the app.

### Elements

#### Areas:
- BlackHole - kills anything that touches it;
- Dust - slows down anything that's passing it;
- Planet - anything that hits it, gets bounced back;
- WormHole - jumps anything that touches it to the next WormHole.

#### Asteroids:
- LooseAsteroid - wonders with a set velocity and direction that only changes on colition;
- CrazyAsteroid - wonders with a set velocity but the direction changes every now and then;
- OrbitalAsteroid - wonders in a circle.

#### NPCs:
- CrazyPlayer - wonders around the game area, leaving dust and changing direction every now and then;
- StraightAheadPlayer - goes straight to the Way-Points, desregarding any obstacle in it's path;
- SmartPlayer - goes to the Way-Points avoiding any obstacles and using wormholes when usefull.


### Extar:
This project was open to extars and, as such, it has 2 extra levels and the human player is able to shoot a ball using the space bar and the key to turn on/off the Sound Effects was changed to "F".

#### level 8:
Has a new npc, the HunterPlayer, that shoots a ball in another players direction. This ball kills any other type of player (npc or not) on contact (like a blackhole).

#### level 9:
This is a multi player level that has two human players. The SecondHumanPlayer is controled with the following keys:
- W - accelarates 1 unit;
- S - decelarates 1 unit;
- A - turns left 10 degrees;
- D - turns right 10 degrees;
- V - shoots a ball.










