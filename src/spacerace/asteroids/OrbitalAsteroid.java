package spacerace.asteroids;

import spacerace.Asteroid;
import spacerace.Coord2D;
import spacerace.GameState;

/**
 * Changes his direction and add "orbit
 * step" at each step
 * The "orbit step" is equals to the initial direction 
 * 
 * @author Joao Rodrigues, Jorge Ferreira
 *
 */
public final class OrbitalAsteroid extends Asteroid {

	/**
	 * A variable to save the value of the orbit direction.
	 */
	private double orbital;
	
	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param orbitStep - Initial Direction;
	 * @param speed - Initial Speed.
	 */
	public OrbitalAsteroid(Coord2D location, double orbitStep, int speed) {
		super(location, orbitStep, speed);
		orbital = orbitStep;
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Changes it's direction to the one it has plus the orbital value.
	 * 
	 * @param gs - The current state of the game.
	 */
	public void step(GameState gs){
		this.setDirection(this.getDirection()+orbital);
	}
}

