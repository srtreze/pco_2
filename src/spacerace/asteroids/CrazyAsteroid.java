package spacerace.asteroids;

import spacerace.Asteroid;
import spacerace.Coord2D;
import spacerace.GameState;

/**
 * 
 * Wanders through space, and changes the direction
 * to randomly every 100 steps
 * 
 * @author Jorge Ferreira, Joao Rodrigues
 *
 */
public final class CrazyAsteroid extends Asteroid {
	
	/**
	 * A variable to count the number of steps
	 */
	private int steps=0;
	
	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param speed - Initial Speed.
	 */
	public CrazyAsteroid(Coord2D location, double direction, int speed) {
		super(location, direction, speed);
	}
	
	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Increments the number of steps.
	 * When it reaches 100, it changes direction to a randomly selected one and sets number of steps to 0.
	 * 
	 * @param gs - The current state of the game.
	 */
	public void step(GameState gs){
		if(steps == 100){
			this.setDirection(Math.random()*360);
			steps = 0;
		}
		++steps;
	}

}
