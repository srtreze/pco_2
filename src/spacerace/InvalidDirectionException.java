package spacerace;

/**
 * Class for the direction exception
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
@SuppressWarnings("serial")
public class InvalidDirectionException extends InvalidLevelException{

	/**
	 * Constructor
	 * 
	 * @param msg - the string that will be returned
	 */
	public InvalidDirectionException(String msg) {
		super(msg);
	}

}
