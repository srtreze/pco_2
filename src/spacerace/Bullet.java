package spacerace;

import spacerace.decorations.Explosion;

/**
 * 
 * An extra class, for the MovingElement Bullet
 * 
 * It explodes and kills any Player it comes with contact with, with the exception of the HunterPlayer.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class Bullet extends MovingElement{

	/**
	 * A variable to count the number of steps
	 */
	private int timer;

	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param speed - Initial Speed.
	 */
	public Bullet(Coord2D location, double direction, int speed) {
		super(location, direction, speed);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Increments the timer.
	 * When it reaches 50, it explodes.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if (timer != 50){
			timer++;
		} else {
			this.die();
			gs.playSound(SoundEffect.EXPLOSION);
			gs.addDecoration(new Explosion(this.getLocation()));
		}
	}
}
