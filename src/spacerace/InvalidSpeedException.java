package spacerace;

/**
 * Class for the speed exception
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
@SuppressWarnings("serial")
public class InvalidSpeedException  extends InvalidLevelException {

	/**
	 * Constructor
	 * 
	 * @param msg - the string that will be returned
	 */
	public InvalidSpeedException(String msg) {
		super(msg);
	}
	
}
