package spacerace.players;

import spacerace.AIPlayer;
import spacerace.Coord2D;
import spacerace.GameState;

/**
 * 
 * A Player that only has one objective, go to his next WayPoint. Ignoring any obstacle in it's way
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class StraightAheadPlayer extends AIPlayer {


	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param referenceSpeed - Initial Speed.
	 */
	public StraightAheadPlayer(Coord2D location, double direction, int referenceSpeed) {
		super(location, direction, referenceSpeed);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Calculates the location of the target WayPoint, and sets it's course to it.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		Coord2D go_to = gs.getWayPointLocation(this.getTargetWayPoint());
		double dir = this.getLocation().directionTo(go_to);
		this.setDirection(dir);
	}

}
