package spacerace.players;

import spacerace.AIPlayer;
import spacerace.Area;
import spacerace.Constants;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.areas.BlackHole;
import spacerace.areas.Dust;
import spacerace.areas.Planet;
import spacerace.areas.WormHole;
import spacerace.areas.EmptyArea;
import spacerace.areas.WayPoint;

/**
 * 
 * A pseudo-Smart Player that only has one objective, go to his next WayPoint.
 * However, contrary to the StraightAheadPlayer, this one avoids colliding with planets and black-holes and moves in straight angles.
 * Verifications concerning WormHoles have not been implemented.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class SmartPlayer extends AIPlayer {
	
	/**
	 * Array containing the 4 possible directions.
	 */
	private double[] dir = {0,90,180,270};

	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param referenceSpeed - Initial Speed.
	 */
	public SmartPlayer(Coord2D location, double direction, int referenceSpeed) {
		super(location, direction, referenceSpeed);
	}

	/**
	 * Direcao
	 */
	@Override
	public void setDirection(double d) {
		super.setDirection(d);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Calculates the location of the target WayPoint. It, then, sees which path is the shortest, avoiding it's obstacles, and chooses it.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		double distance;
		double bestDistance = Double.MAX_VALUE;
		double bestDirection = this.getDirection();
		Coord2D originalP = Constants.normalize(getLocation());
		int i = 0;
		while( i < dir.length){
			Coord2D view = originalP.displace(Constants.ELEM_WIDTH*1.1, dir[i]);
			Area view_area = gs.getArea(view);
			if(view_area instanceof EmptyArea){
				distance = view_area.getLocation().distanceTo(gs.getWayPointLocation(this.getTargetWayPoint()));
				if(distance < bestDistance){
					bestDistance = distance;
					bestDirection = dir[i];
				}
			}else if(view_area instanceof Dust){
				distance = view_area.getLocation().distanceTo(gs.getWayPointLocation(this.getTargetWayPoint()))*1.25;
				if(distance < bestDistance){
					bestDistance = distance;
					bestDirection = dir[i];
				}
			}else if(view_area instanceof WormHole){
				distance = ((WormHole) view_area).getExit().distanceTo(gs.getWayPointLocation(this.getTargetWayPoint()));
				if(distance < bestDistance){
					bestDistance = distance;
					bestDirection = dir[i];
				}

			}else if(view_area instanceof WayPoint){
				distance = view_area.getLocation().distanceTo(gs.getWayPointLocation(this.getTargetWayPoint()));
				if(distance < bestDistance){
					bestDistance = distance;
					bestDirection = dir[i];
				}
			}
		i++;
		}
		this.setDirection(bestDirection);
	}
}
