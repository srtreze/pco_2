package spacerace.players;

import spacerace.Bullet;
import spacerace.Constants;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.HumanPlayerCommand;
import spacerace.Player;


/**
 * 
 * A Player that's controlled by the user. It can accelerate, decelerate, turn left, turn right and shoot bullets
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class HumanPlayer extends Player {

	/**
	 * Constructor
	 * 
	 * By default, the initial speed is 0
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 */
	public HumanPlayer(Coord2D location, double direction) {
		super(location, direction, 0);
	}
 
	/**
	 * Handles the command given.
	 * 
	 * Option:
	 * 
	 * SPEED_UP - accelerates by one unity, maximum 10;
	 * SPEED_DOWN - decelerates by one unity, minimum 0;
	 * TURN_LEFT - turns left, by decreasing 10 degrees;
	 * TURN_RIGHT - turns right, by increasing 10 degrees;
	 * FIRE - fires a bullet element
	 * 
	 * @param c - The command given
	 * @param gs - The current state of the game
	 */
	public final void handleCommand(HumanPlayerCommand c, GameState gs) {
		switch(c) {
	      	case SPEED_UP:
	      		if (getSpeed() < Constants.MAX_SPEED) {
	      			setReferenceSpeed(getSpeed() + 1);
	      		}
	      		break;
	      	case SPEED_DOWN:
	      		if (getSpeed() > 0) {
	      			setReferenceSpeed(getSpeed() - 1);
	      		}
	      		break;
	      	case TURN_LEFT:
	      		setDirection(getDirection() - 10 );
	      		break;
	      	case TURN_RIGHT:
	      		setDirection(getDirection() + 10 );
	      		break;
	        case FIRE:
	        	gs.addBullet(new Bullet(this.getLocation().displace(Constants.ELEM_WIDTH * 1.1, getDirection()), getDirection(), 15));
	        	break; 
		}
	}
}
