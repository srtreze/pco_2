package spacerace.players;

import spacerace.AIPlayer;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.areas.Dust;
import spacerace.areas.EmptyArea;

/**
 * 
 * Player that walks randomly through the map leaving
 * a dust each 50 step if that area is an EmptyArea
 * 
 * @author Joao Rodrigues, Jorge Ferreira
 *
 */
public final class CrazyPlayer extends AIPlayer {

	/**
	 * A variable to count the number of steps
	 */
	private int steps;

	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param referenceSpeed - Initial Speed.
	 */
	public CrazyPlayer(Coord2D location, double direction, int referenceSpeed) {
		super(location, direction, referenceSpeed);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Increments the number of steps.
	 * When it reaches 50, if the Area where hi's at is empty, it places a Dust element.
	 * He also changes direction to a randomly selected one and sets number of steps to 0.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if(steps == 50){
			if(gs.getArea(this.getLocation()) instanceof EmptyArea){
				gs.addArea(new Dust(this.getLocation()));
			}
			this.setDirection(Math.random()*360);
			steps = 0;
		}
		++steps;
	}
}

