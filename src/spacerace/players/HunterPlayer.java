package spacerace.players;

import spacerace.AIPlayer;
import spacerace.Bullet;
import spacerace.Coord2D;
import spacerace.GameState;

/**
 * 
 * An extra class, for a player that "hunts" the human player. Only available on level 8 (level_8.txt)
 * 
 * A Player that moves randomly. It every 50 steps, it shoots a bullet in the direction of the human player.
 * The HunterPlayer is not affected by bullets
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class HunterPlayer extends AIPlayer{
	
	/**
	 * A variable to count the number of steps
	 */
	private int steps;

	/**
	 * Constructor
	 * 
	 * It's initial direction is random.
	 * 
	 * @param location - Initial Location;
	 * @param referenceSpeed - Initial Speed;
	 */
	public HunterPlayer(Coord2D location, int referenceSpeed) {
		super(location, Math.random()*360, referenceSpeed);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Increments the number of steps.
	 * When it reaches 50, it shoots a bullet in the players direction, changes direction to a randomly selected one and sets number of steps to 0.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if(steps == 50){
			
			if(gs.getHumanPlayer().isAlive()){
				
				double dir = this.getLocation().directionTo(gs.getHumanPlayer().getLocation());
				
				gs.addBullet(new Bullet(this.getLocation(), dir, 15));
			}
			
			this.setDirection(Math.random()*360);
			steps = 0;

		}
		++steps;

	}

}
