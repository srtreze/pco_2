package spacerace;

/**
 * Class for the location exception
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
@SuppressWarnings("serial")
public class InvalidLocationException extends InvalidLevelException{

	/**
	 * Constructor
	 * 
	 * @param msg - the string that will be returned
	 */
	public InvalidLocationException(String msg) {
		super(msg);
	}
}
