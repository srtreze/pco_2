package spacerace;

/**
 * 
 * Enumeration of the second human player's controls.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 * 
 */public enum SecondHumanPlayerCommand {
	/**
	 * Speed up.
	 */
	SPEED_UP,
	/**
	 * Speed down.
	 */
	SPEED_DOWN,
	/**
	 * Turn left.
	 */
	TURN_LEFT,
	/**
	 * Turn right.
	 */
	TURN_RIGHT,
	/**
	 * Fires a Bullet element
	 */
	FIRE;

}
