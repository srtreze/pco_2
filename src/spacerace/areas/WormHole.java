package spacerace.areas;

import spacerace.Area;
import spacerace.Constants;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.MovingElement;
import spacerace.SoundEffect;

/**
 * 
 * A static object that teleports, to it's exit, any MovingElement object that comes in contact with him. 
 * When it teleports an object, it plays the ENTER_WORMHOLE sound
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class WormHole extends Area {

	/**
	 * Stores the coordinates of the exit
	 */
	private Coord2D exit;

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 * @param exit - the location of the exit form the WormHole
	 */
	public WormHole(Coord2D location, Coord2D exit) {
		super(location);
		this.exit = exit;
	}

	/**
	 * 
	 * Returns the exit coordinates
	 * 
	 * @return exit
	 */
	public Coord2D getExit() {
		return exit;
	}

	/**
	 * 
	 * Implements and overrides the function interactWith() of the Superclass. Executes whenever it interacts with a MovingElement object.
	 * 
	 * Teleports to it's exit, the MovingElement object that collided with him.
	 * Plays the ENTER_WORMHOLE sound.
	 * 
	 * @param gs - the game state;
	 * @param e - the MovingElement that collided with it
	 * 
	 */
	@Override
	public void interactWith(GameState gs, MovingElement e) {
		e.setLocation(exit.displace(Constants.ELEM_WIDTH * 1.1, e.getDirection()));
		gs.playSound(SoundEffect.ENTER_WORMHOLE);
	}
}
