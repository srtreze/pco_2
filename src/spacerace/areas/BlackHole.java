package spacerace.areas;

import spacerace.Area;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.MovingElement;
import spacerace.SoundEffect;
import spacerace.decorations.Explosion;

/**
 * 
 * A static object that "kills" any MovingElement object that comes in contact with him. 
 * When it "kills", it makes an explosion and plays the EXPLOSION sound
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class BlackHole extends Area {

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 */
	public BlackHole(Coord2D location) {
		super(location);
	}

	/**
	 * 
	 * Implements and overrides the function interactWith() of the Superclass. Executes whenever it interacts with a MovingElement object.
	 * 
	 * Kills the MovingElement object that collided with him.
	 * Adds the Decoration Explosion, and play a sound with the same name.
	 * 
	 * @param gs - the game state;
	 * @param e - the MovingElement that collided with it
	 * 
	 */
	@Override
	public void interactWith(GameState gs, MovingElement e) {
		e.die();
		gs.playSound(SoundEffect.EXPLOSION);
		gs.addDecoration(new Explosion(e.getLocation()));
	}

}
