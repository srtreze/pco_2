package spacerace.areas;

import spacerace.Area;
import spacerace.Constants;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.MovingElement;


/**
 * 
 * A static object that slows down any MovingElement object while it passes through it. 
 * 
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class Dust extends Area {

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 */
	public Dust(Coord2D location) {
		super(location);
	}

	/**
	 * 
	 * Implements and overrides the function interactWith() of the Superclass. Executes whenever it interacts with a MovingElement object.
	 * 
	 * Slows down any MovingElement object while it passes through it.
	 * 
	 * 
	 * @param gs - the game state;
	 * @param e - the MovingElement that collided with it
	 * 
	 */
	@Override
	public void interactWith(GameState gs, MovingElement e) {
		e.setMaximumSpeed(Constants.MAX_SPEED_IN_DUST);
	}
	
}

