package spacerace.areas;

import spacerace.Area;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.MovingElement;
import spacerace.SoundEffect;



/**
 * 
 * A static object that bounces any MovingElement object that comes in contact with him. 
 * When it deflects an object, it plays the COLLISION sound
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class Planet extends Area {

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 */
	public Planet(Coord2D location) {
		super(location);
	}

	/**
	 * 
	 * Implements and overrides the function interactWith() of the Superclass. Executes whenever it interacts with a MovingElement object.
	 * 
	 * Bounces the MovingElement object that collided with him.
	 * Plays the COLLISON sound.
	 * 
	 * @param gs - the game state;
	 * @param e - the MovingElement that collided with it
	 * 
	 */
	@Override
	public void interactWith(GameState gs, MovingElement e) {
		e.setDirection(e.getDirection()-180);
		gs.playSound(SoundEffect.COLLISION);
	}
	
}
