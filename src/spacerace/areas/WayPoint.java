package spacerace.areas;

import spacerace.Area;
import spacerace.Coord2D;
import spacerace.GameState;
import spacerace.MovingElement;
import spacerace.Player;
import spacerace.SoundEffect;
import spacerace.decorations.WayPointReached;

/**
 * 
 * A static object that has it's own index.
 * It's like a check point that the Player elements have to reach, in order of index.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class WayPoint extends Area {

	/**
	 * A variable that stores the index value of the WayPoint
	 */
	private int index;

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 * @param index - The index of the WayPoint
	 */
	public WayPoint(Coord2D location, int index) {
		super(location);
		this.index=index;
	}

	/**
	 * 
	 * Returns the index value
	 * 
	 * @return index
	 */
	public int getIndex() {
		return index;
	}


	/**
	 * 
	 * Implements and overrides the function interactWith() of the Superclass. Executes whenever it interacts with a MovingElement object.
	 * 
	 * Whenever a Player element comes in contact with it, if that element has this object's index as target,it increments it's own.
	 * If it's the last WayPoint, it plays the PLAYER_WON sound. Else, it plays the WAYPOINT_REACHED sound.
	 * 
	 * @param gs - the game state;
	 * @param e - the MovingElement that collided with it
	 * 
	 */
	@Override
	public void interactWith(GameState gs, MovingElement e) {
		if (e instanceof Player && ((Player) e).getTargetWayPoint() == this.index){
			((Player) e).advanceToNextWayPoint();
			gs.addDecoration(new WayPointReached(this.getLocation()));
			if(((Player) e).getTargetWayPoint() != gs.numberOfWaypoints()){
				gs.playSound(SoundEffect.WAYPOINT_REACHED);
			} else {
				gs.playSound(SoundEffect.PLAYER_WON);
			}
		}
	}

	/**
	 * Yield way-point index as label.
	 * @return A string for the way-point index.
	 */
	@Override
	public String getLabel() {
		return String.valueOf(getIndex());
	}

}
