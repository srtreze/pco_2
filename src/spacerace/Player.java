package spacerace;

/**
 * 
 * The class for players. Extends MovingElement.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 * 
 *
 */
public abstract class Player extends MovingElement {

	/**
	 * An integer to know what's the target
	 */
	private int target = 0;

	/**
	 * Constructor
	 * 
	 * @param location - Initial Location;
	 * @param direction - Initial Direction;
	 * @param referenceSpeed - Initial Speed.
	 */
	public Player(Coord2D location, double direction, int referenceSpeed) {
		super(location, direction, referenceSpeed);

	}

	/**
	 * Gets the index to know which WayPoint is the target
	 * 
	 * @return the index
	 */
	public final int getTargetWayPoint() {
		return target;
	}

	/**
	 * Increments the index
	 */
	public final void advanceToNextWayPoint() {
		target++;
	}

	/**
	 * Yield target way-point as label.
	 * @return The target way-point as a string.
	 */
	@Override
	public String getLabel() {
		return String.valueOf(getTargetWayPoint());
	}
}
