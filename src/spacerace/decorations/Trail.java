package spacerace.decorations;

import java.awt.Color;
import java.awt.Graphics;

import spacerace.Coord2D;
import spacerace.Decoration;
import spacerace.GameState;

/**
 * 
 * A trail left behind by every MovingElement object.
 * The size gets progressively smaller, in accordance with the steps. "Dies" after 20 steps.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class Trail extends Decoration {

	/**
	 * A variable with the size of the object
	 */
	private int size = 20;

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 */
	public Trail(Coord2D location) {
		super(location);
	}


	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Decrements the size. When it reaches 0, it "kill's" itself.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if (size != 0){
			size--;
		} else {
			this.die();
		}
	}


	/**
	 * 
	 * Implements and overrides the function draw() of the Superclass. Executes every step.
	 * 
	 * Draws a small circle behind every MovingElement object.
	 * It's color is green and it's opacity value is equal to it's size.
	 * 
	 * @param g - The Graphics element that will be painted.
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(new Color(0, 128, 0, size));
		g.fillOval((int)getLocation().getX()-10, (int)getLocation().getY()-10, size, size);
	}

}
