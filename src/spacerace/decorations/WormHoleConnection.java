package spacerace.decorations;

import java.awt.Color;
import java.awt.Graphics;

import spacerace.Coord2D;
import spacerace.Decoration;
import spacerace.GameState;
import spacerace.areas.WormHole;

/**
 * 
 * A decoration for the WormHole elements.
 * It creates blinking circles in the WormHole elements synchronized with a blinking line that connect them.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public final class WormHoleConnection extends Decoration {

	/**
	 * A variable that keeps the value of alpha
	 */
	private int alpha = 0;
	
	/**
	 * A variable to save the coordinates of the WorHole's exit
	 */
	private Coord2D exit;

	/**
	 * Constructor
	 * 
	 * @param w - The WormHole to which this decoration will be attach
	 */
	public WormHoleConnection(WormHole w) {
		super(w.getLocation());
		exit = w.getExit();
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Increments the number of steps, 10 at the time.
	 * When it reaches 100, it sets back to 0.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if (alpha <= 100){
			alpha+=10;
		} else {
			alpha = 0;
		}
	}

	/**
	 * 
	 * Implements and overrides the function draw() of the Superclass. Executes every step.
	 * 
	 * Draws 2 small circles in each of the WormHoles (the one received in the constructor, and it's exit). They're size is 10x10.
	 * Also draws a line connecting both circles.
	 * 
	 * They're color is white and shifts due to alpha.
	 * 
	 * @param g - The Graphics element that will be painted.
	 */
	@Override
	public void draw(Graphics g) {
		g.setColor(new Color(255, 255, 255, alpha));
		g.fillOval((int)getLocation().getX()-5, (int)getLocation().getY()-5, 10, 10);
		g.fillOval((int)exit.getX()-5, (int)exit.getY()-5, 10, 10);
		g.drawLine((int)getLocation().getX(), (int)getLocation().getY(), (int)exit.getX(), (int)exit.getY());
	}

}
