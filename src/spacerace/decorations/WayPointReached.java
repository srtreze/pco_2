package spacerace.decorations;

import java.awt.Color;
import java.awt.Graphics;

import spacerace.Coord2D;
import spacerace.Decoration;
import spacerace.GameState;


/**
 * 
 * A decoration for the WayPoint elements. Only created when a Player reaches the correct WayPoint.
 * It creates a blinking circle in the WayPoint element. Blinks every step until the 20th, the it dies.
 * 
 * @author group039, João Miguel Rodrigues e Jorge Ferreira, DI/FCUL, 2015
 *
 */
public class WayPointReached extends Decoration {
	
	/**
	 * A variable to know when to "kill" the decoration
	 */
	private int timer = 20;

	/**
	 * Constructor
	 * 
	 * @param location - Initial and final location of the decoration
	 */
	public WayPointReached(Coord2D location) {
		super(location);
	}

	/**
	 * 
	 * Implements and overrides the function steps() of the Superclass. Executes every step.
	 * 
	 * Decrements the timer. When it reaches 0, it "kill's" itself.
	 * 
	 * @param gs - The current state of the game.
	 */
	@Override
	public void step(GameState gs) {
		if (timer != 0){
			timer--;
		} else {
			this.die();
		}
	}
	
	/**
	 * 
	 * Implements and overrides the function draw() of the Superclass. Executes every step.
	 * 
	 * Draws a circle that completely covers the WayPoint.
	 * It's color is green and blinks between to transparencies, 10 and 50.
	 * 
	 * @param g - The Graphics element that will be painted.
	 */
	@Override
	public void draw(Graphics g) {
		if(timer%2 == 0){
			g.setColor(new Color(0, 128, 0, 50));
		} else {
			g.setColor(new Color(0, 128, 0, 10));
		}
		g.fillOval((int)getLocation().getX()-25, (int)getLocation().getY()-25, 50, 50);
	}


}

